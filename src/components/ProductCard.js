import { useState, useEffect, useContext } from 'react'
import { Card, Col, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import AppContext from '../AppContext'


export default function ProductCard(props) {

   const { user } = useContext(AppContext);
   const{productId} = useParams();

    //object destructuring
    const {breakpoint, productProp} = props
    const {name, description, price, _id} = productProp



    return (
        <Col xs={12} md={breakpoint} className="mt-4" >
        <Card className="card1">
            <Card.Body>
                <Card.Title className="text-center card2">{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text className="card3">{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>

            </Card.Body>
            <Card.Footer>
                            { user.id !== null
                                ?
                                <div className= "d-grid gap 2">
                                    <Button as={Link} to={`/viewProduct/${_id}`} className= "btn-primary mb-2">
                                        View
                                    </Button>

                                </div>
                                :
                                <>
                                    <p>Not yet registered? <Link to="/register">Register Here</Link></p>
                                    <Link className="btn btn-danger" to="/login">Log In</Link>
                                </>
                            }
                        </Card.Footer>

        </Card>
        </Col>

    )
}